This is a small script to back up your fediverse [Mastodon/etc.] posts using the client API, for when you can't use server-side post export facilities. Like say if you're on a Pleroma instance.

## Dependencies

You'll need curl, jq, and zsh installed.

## Usage

```
fedibackup [--proxy proxy-URL] URL out-file [token-file [max # of batches]]
```

The URL you need is `https://<your-instance>/api/v1/accounts/<account-ID>`.

If you're on Mastodon, you can get your account ID through the web interface: open your profile, then grab the number out of the URL bar.  
If you're on Pleroma, you can do the same through MastoFE.  
I'm not sure how to get it for other server software.

> ⚠ Don't use the "max number of batches" option except for quick tests. It will only download your most recent N * (whatever the server decided the batch size should be) posts.

#### Authorization

The script will only back up public posts unless you can get an OAuth token for it. And if your instance uses authenticated fetch, even that won't work and you _need_ the token to do anything.

If you're using Mastodon, you can pop over to the Development tab in your account settings... but if you're here, you're probably not using Mastodon! Probably the easiest way is to log into your account with [Whalebird](https://whalebird.social). Then you can do `jq --tab . ~/.config/Whalebird/db/account.db` and nab the access token from there.

## License

Public domain, go wild! See [COPYING.md](COPYING.md) for details.
